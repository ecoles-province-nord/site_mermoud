---
title: "Info"
description: Info ecole
omit_header_text: true
type: page
menu: main
date: 2020-08-10T16:24:41+11:00
featured_image: "/images/logo4.png"
draft: false
---


**Ecole Charle Mermoud**

* Localisation

![plan](/images/plan.jpg)


* Horraires :
    + Ouverture des Portes        : 7h35
    + Debut des Cours             : 7h45
    + Pause meridienne            : 11h15
    + Reprise des Cours           : 13h15
    + Fin des Cours               : 15h45 / Vendredi: 15h15

